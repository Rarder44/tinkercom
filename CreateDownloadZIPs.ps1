$compress = @{
  Path = ".\TinkerCOM\bin\x64\Release\CefLib.dll", ".\TinkerCOM\bin\x64\Release\ExtendCSharp.dll",".\TinkerCOM\bin\x64\Release\TinkerCOM.exe"
  CompressionLevel = "Optimal"
  DestinationPath = ".\_DOWNLOAD\TinkerCOM x64.zip"
}
Compress-Archive @compress

$compress2 = @{
  Path = ".\TinkerCOM\bin\x86\Release\CefLib.dll", ".\TinkerCOM\bin\x86\Release\ExtendCSharp.dll",".\TinkerCOM\bin\x86\Release\TinkerCOM.exe"
  CompressionLevel = "Optimal"
  DestinationPath = ".\_DOWNLOAD\TinkerCOM x86.zip"
}
Compress-Archive @compress2