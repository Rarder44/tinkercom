﻿# TinkerCOM
Programma per permettere di collegare un arduino virtuale su Tinkercad ad una porta seriale locale

il programma non è in fase di BETA, nemmeno di ALPHA, e nemmeno di PRE-ALPHA.  
diciamo che il programma non dovrebbe quasi neanche esistere, è già un miracolo se funziona.

[---> CLICCARE QUA PER ANDARE AL LINK DI DOWNLOAD  <---](https://bitbucket.org/Rarder44/tinkercom/src/master/_DOWNLOAD/)

## Uso del programma:
1. Navigare fino al progetto  
1. una volta sulla pagina del progetto CONSIGLIO di modificare il nome della borad di arduino ( impostare ad esempio ARDUINO1 )  
1. Cliccare sul pulsante "Get Names"  
	* Nel riquadro grigio dovrebbero comparire il nome delle board presenti nel progetto  
1. sotto ogni board è presente una ComboBox dove è possibile selezionare la porta COM da associargli ( lasciare "-" se non si vuole associare nessuna COM)  
1. cliccare sul pulsante "Attiva"  
  
A questo punto il pallino sul pulsante dovrebbe divetare verde e comparire la scritta "Disattiva"; questo vuol dire che il programma è in funzione.  
E'possibile Disattivare l'associazione e cambiare le COM a piaciemento.  
  
Cambiare pagina/progetto o anche solo ricaricarla va a rimuovere l'associazione COM-Arduino, occorre quindi ripetere la procedura dal punto 3   



## Virtualizzazione COM
E' possibile virtualizzare le porte COM e collegare quindi più arduini virtuali o un arduino virtuale con un programma usando [com0com](http://com0com.sourceforge.net/)  
PER EVITARE DI DOVER DISABILITARE IL SECURE BOOT, BASTA SCARICARE LA VERSIONE "com0com-2.2.2.0-x64-fre-signed.zip"  
[Link al download](https://sourceforge.net/projects/com0com/files/com0com/2.2.2.0/com0com-2.2.2.0-x64-fre-signed.zip/download)  

Creare prima la coppia di porte virtuali associate:  

1. "Add Pair"  
1. (Opzionale) modificare il numero di porta dalla textbox-> Apply  

Da TinkerCOM selezionare la/le porte COM virtualizzate  
  
Esempio:  
com0com crea    COM20 <-> COM 21  
Arduino monitor -> COM20 <-> COM21 <- TinkerCOM  
( mette in comunicazione il monitor di arduino con arduino virtualizzato)  
  
  
## Ricompilazione
Il programma è scritto in c# (WPF).

~~ATTENZIONE: per uno scarso allineamento degli astri, la versione a 64bit non funziona, occorre compilare la versione a x86 ( E NON A AnyCPU!!!  )~~  
MA ATTENZIONE!!! GIOVE SI E' ALLINEATO CON SATURNO E LA COSTELLAZIONE DI ORIONE, E GRAZIE ALLA MAGIA DELLA POLVERE DI FATA ORA SI COMPILA ANCHE A x64!  
( in realtà bastava settare x64 all'interno delle opzioni di compilazione del singolo progetto... ma è più folkloristico pensare che sia [MAGIA](https://media.tenor.com/images/5bcb5056e6dfe7f757018ecaa8a4b868/tenor.gif) )

Utilizza le seguenti librerie:  

* [ExtendCSharp](https://github.com/Rarder44/ExtendCSharp)  
* [CefLib](https://bitbucket.org/Rarder44/ceflibrary/src/master/)  
  
Occorre quindi:  

  -  copiare con git questo progetto e i progetti delle librerie nella stessa cartella contenitore  
  -  controllare se CefLib ha i paccketti NuGet installati correttamente  
    *  aprire SOLO il progetto CefLib  
    *  aprire il menu dei pacchetti NuGet  
    *  controllare se i pacchetti sono installati correttamente ( se cosi non fosse, dovrebbe comparire un avviso per la re-installazione)
  -  per sicurezza, fare la build della libreria e verificare la compilazione
  -  aprire la soluzione TinkerCOM, dovrebbe già rilevare i progetti delle DLL ( se così non fosse, basta specificagli la posizione del file di progetto)
  -  compilare

