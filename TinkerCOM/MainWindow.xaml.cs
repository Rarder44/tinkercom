﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CefLib.Controls;
using TinkerCOM.Classes;
using TinkerCOM.ScriptExecuters;
using ExtendCSharp;
using TinkerCOM.Controls;
using CefLib.Classes;
using ExtendCSharp.Services;

namespace TinkerCOM
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TinkercadScript jStoCS = new TinkercadScript();
        COM_watchers_manager COM_manager;
        public MainWindow()
        {

            Dependencyer.CheckAndFixDependencies();

            InitializeComponent();
            browser.DocumentCompleted += Browser_DocumentCompleted;
            COM_manager = new COM_watchers_manager(jStoCS);
            browser.BaseBrowser.RegistScript(jStoCS);

            browser.NavigateAndWait("https://www.tinkercad.com/");
        }

        async private void Browser_DocumentCompleted(object sender, EventArgs e)
        {
            //in qualsiasi caso, tolgo le associazioni correnti e spengo i  COM_watcher
            
            if (COM_manager.Running)
            {
                Toggle_COM();
            }
            arduiniCOM_panel.ClearInvoke();
            COM_JS_Association.Clear();


            Regex r = new Regex("https://www.tinkercad.com/things/.*/editel");
            string url = browser.Url;
            if (r.IsMatch(url))
            {  
                await jStoCS.Bind();
                controlPanel.IsEnabledInvoke(true);
            }
            else
            {
                controlPanel.IsEnabledInvoke(false);
            }
        }

        async private void Button_Click(object sender, RoutedEventArgs e)
        {
            await browser.NavigateAndWait("https://www.tinkercad.com/things/a20LEXViTYU-super-fyyran/editel");
        }


        private void CreateArduinoCOMassociationPanel(String[] Names)
        {
            arduiniCOM_panel.Children.Clear();
            foreach (String name in Names)
            {
                COMassociatonPanel c = new COMassociatonPanel(name);
                c.Width = arduiniCOM_panel.Width;
                arduiniCOM_panel.Children.Add(c);
            }
           
        }



        async private void RefreshName()
        {
            if (COM_manager.Running)
            {
                Toggle_COM();
            }
            arduiniCOM_panel.ClearInvoke();
            COM_JS_Association.Clear();
            CreateArduinoCOMassociationPanel(await jStoCS.GetArduiniNames());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            RefreshName();
        }



        private void Toggle_COM()
        {
            this.Dispatcher.Invoke(() =>
            {
                if (COM_manager.Running)
                {
                    COM_manager.StopAll();
                    btn_Attiva_Disattiva_led.Fill = new SolidColorBrush(Colors.Red);
                    btn_Attiva_Disattiva_text.Text = "Attiva";
                    arduiniCOM_panel.IsEnabled = true;
                }
                else
                {
                    String err = COM_manager.StartAll();
                    if (err==null)
                    {
                        btn_Attiva_Disattiva_led.Fill = new SolidColorBrush(Colors.LightGreen);
                        btn_Attiva_Disattiva_text.Text = "Disattiva";
                        arduiniCOM_panel.IsEnabled = false;
                    }
                    else
                    {
                        MessageBox.Show(err, "Errore durante l'apertura della porta seriale", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            });          
            
        }
        private void btn_Attiva_Disattiva_Click(object sender, RoutedEventArgs e)
        {
            Toggle_COM();
        }


        KeyService ks = new KeyService();
        private void browser_KeyDown(object sender, KeyEventArgs e)
        {
            /*
             * TODO: cerco di cambiare questo aborto
             * il browser riceve il key code della graffa ( OEM1+modificatore per aperta e OEMPlus+modificatore per chiusa ) 
             * ma sa il cazzo perchè non la visualizza ( la valuta come un key non corretto?? BHA! ) 
             * quindi filtro questi 2 char ( convertendo il key in char per mitigare i problemi di tastiera e localizzazione ) 
             * e se corrispondono invio un keyEvent diretto al browser con il codice corrispondente ( E SA IL CAZZO PERCHE' COSI GLI PIACE! ) 
             */
            char c = ks.GetCharFromKey(e.Key);
            if(c=='{' || c=='}')
            {
                browser.BaseBrowser._Cast<ChromeBrowser_WPF>().SendKey((int)c);
                e.Handled = true;
            }
        }
    }
}
