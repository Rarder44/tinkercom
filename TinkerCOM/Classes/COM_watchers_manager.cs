﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinkerCOM.ScriptExecuters;

namespace TinkerCOM.Classes
{
    /// <summary>
    /// Gestore di watch multipli ( più arduini nel progetto )
    /// </summary>
    class COM_watchers_manager
    {
        TinkercadScript js;
        Dictionary<String, COM_watcher> watchers;

        /// <summary>
        /// Identifica lo stato del manager 
        /// </summary>
        public bool Running { get; private set; }


        public COM_watchers_manager(TinkercadScript javascriptBridge)
        {
            js = javascriptBridge;
            js.OnSerialReceived += Js_OnSerialReceived;
            watchers = new Dictionary<string, COM_watcher>();
        }

        


        /// <summary>
        /// Avvia tutti i watcher basandosi sulle associazioni gestite dalla 
        /// </summary>
        /// <returns></returns>
        public String StartAll()
        {
            if (Running)
                return null;

            watchers.Clear();
            try
            {
                foreach (KeyValuePair<string, string> arduinoCOM in COM_JS_Association.GetAssociations())
                {
                    watchers[arduinoCOM.Key] = new COM_watcher(arduinoCOM.Key, arduinoCOM.Value);
                    watchers[arduinoCOM.Key].Start();
                    watchers[arduinoCOM.Key].OnMessageReceived += COM_watchers_manager_OnMessageReceived;
                }
                Running = true;
                return null;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                StopAll(true);
                return ex.Message;
            }

        }


        public void StopAll(bool force = false)
        {
            if (!Running && !force)
                return;
            foreach (COM_watcher arduinoCOM in watchers.Values)
            {
                try
                {
                    arduinoCOM.Stop();
                }
                catch (Exception ex)
                {

                }
            }
            Running = false;
        }

       

        //da js a seriale
        private void Js_OnSerialReceived(string arduino, byte dataReceived)
        {
            if (!Running)
                return;
            
            if (watchers.ContainsKey(arduino))
                watchers[arduino].SendMessage(Convert.ToChar(dataReceived).ToString());
        }

        //da seriale a js
        private void COM_watchers_manager_OnMessageReceived(string ArduinoName, string Message)
        {
            if (!Running)
                return;

            js.SendMessage(ArduinoName, Message);
        }
    }
}
