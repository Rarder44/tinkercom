﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinkerCOM.Classes
{
    /// <summary>
    /// Classe che gestisce la comunicazione seriale
    /// </summary>
    class COM_watcher
    {
        String _ArduinoName, _COMport;
        SerialPort COM;

        public delegate void OnMessageReceivedEventArgs(String ArduinoName, String Message);
        /// <summary>
        /// Evento che espone la ricezione di un messaggio sulla porta seriale
        /// </summary>
        public event OnMessageReceivedEventArgs OnMessageReceived;


        public COM_watcher(String ArduinoName,String COMport)
        {
            //TODO: leggo la velocità dirattamente dal sorgente dell'arduino e lo passo come parametro
            _ArduinoName = ArduinoName;
            _COMport = COMport;
            COM = new SerialPort(COMport, 9600);
            COM.DataReceived += COM_DataReceived;
        }


        public void Start()
        {
            COM.Open();
        }
        public void Stop()
        {
            if(COM.IsOpen)
                COM.Close();
        }


        private void COM_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string s = COM.ReadExisting();
            OnMessageReceived?.Invoke(_ArduinoName, s);
        }



        /// <summary>
        /// Invia un messaggio sulla porta seriale corrente
        /// </summary>
        /// <param name="Message"></param>
        public void SendMessage(String Message)
        {
            if (!COM.IsOpen)
                return;
            COM.Write(Message);
        }
        /// <summary>
        /// Invia un messaggio sulla porta seriale corrente
        /// </summary>
        /// <param name="Message"></param>
        public void SendMessage(byte[] Message)
        {
            if (!COM.IsOpen)
                return;
            COM.Write(Message,0,Message.Length);
        }



    }
}
