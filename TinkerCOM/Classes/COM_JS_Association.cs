﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinkerCOM.Classes
{
    static class COM_JS_Association
    {
        /// <summary>
        /// lista interna per la gestione associazione NomeArduino - COM port
        /// </summary>
        private static Dictionary<String, String> Arduino_COM = new Dictionary<string, string>();

        /// <summary>
        /// Rimuove tutte le associazioni
        /// </summary>
        public static void Clear()
        {
            Arduino_COM.Clear();
        }
        /// <summary>
        /// Ritorna tutte le porte COM del sistema
        /// </summary>
        /// <returns></returns>
        public static String[] GetPorts()
        {
            return SerialPort.GetPortNames();
        }

        /// <summary>
        /// Ritorna tutte le porte non attualmente già utilizzate da altri arduini
        /// </summary>
        /// <returns></returns>
        public static String[] GetFreePorts()
        {
            return GetPorts().AsEnumerable<string>().Where((s) => { return !Arduino_COM.ContainsValue(s); }).ToArray();
        }

        /// <summary>
        /// Ritorna un IEnumerable ( non modificabile ) delle associaizoni 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string,string>> GetAssociations()
        {
            return Arduino_COM.AsEnumerable();
        }

        /// <summary>
        /// Ritorna la COM in base al nome dell'arduino
        /// </summary>
        /// <param name="ArduinoName"></param>
        /// <returns>COMXX se è presente, altrimenti null</returns>
        public static String GetCOM(String ArduinoName)
        {
            if (Arduino_COM.ContainsKey(ArduinoName))
                return Arduino_COM[ArduinoName];
            return null;
        }

        /// <summary>
        /// Imposta una associazione Nome Aduino - Porta COM
        /// </summary>
        /// <param name="ArduinoName">Nome della porta</param>
        /// <param name="ArduinoCOM">nome della com oppure null per rimuovere un associazione presente</param>
        /// <returns>true in caso di successo | false in caso di porta già occupata ( o altro errore ) </returns>
        public static bool SetCOM(String ArduinoName, string ArduinoCOM)
        {
            if (ArduinoCOM == null)
            {
                Arduino_COM.Remove(ArduinoName);
                return true;
            }


            if (GetFreePorts().Contains(ArduinoCOM))
            {
                Arduino_COM[ArduinoName] = ArduinoCOM;
                OnArduinoCOMchanged?.Invoke(ArduinoName, ArduinoCOM);
                return true;
            }
            return false;
        }


        /// <summary>
        /// Check se il nome dell'arduino ha già una com assiciata
        /// </summary>
        /// <param name="ArduinoName"></param>
        /// <returns></returns>
        public static bool ArduinoAlreadyAssociated(String ArduinoName)
        {
            return Arduino_COM.ContainsKey(ArduinoName);
        }



        public delegate void OnArduinoCOMchangedEventHandler(String ArduinoName, String ArduinoCOM);
        /// <summary>
        /// Evento lanciato quando una COM viene occupata
        /// </summary>
        public static event OnArduinoCOMchangedEventHandler OnArduinoCOMchanged;


    }
}
