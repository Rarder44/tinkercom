﻿using CefLib.Interfaces;
using CefLib.Classes;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefLib.Controls;
using ExtendCSharp.Services;
using ExtendCSharp;
namespace TinkerCOM.ScriptExecuters
{

    /// <summary>
    /// Classe per la gestione dello script js su Tinkercad
    /// </summary>
    public class TinkercadScript : IScriptExecuter
    {
        public delegate void OnSerialReceivedEventHandler(String arduino, byte dataReceived);
        /// <summary>
        /// Evento che viene lanciato quando viene ricevuto un messaggio da un arduino
        /// </summary>
        public event OnSerialReceivedEventHandler OnSerialReceived;

        public bool jsEventAlreadyBinded { get; private set; } = false;

        public TinkercadScript()
        {

        }


        /// <summary>
        /// Permette di injectare lo script
        /// </summary>
        /// <returns></returns>
        public async Task Bind()
        {
            if (!IsRegistered())
                throw new ScriptNotRegisterException(this);

            if (!jsEventAlreadyBinded)
            {
                browser.JavascriptMessageReceived += Browser_JavascriptMessageReceived;
                jsEventAlreadyBinded = true;
            }

            //Carico lo script per l'inject
            String script = new ResourcesService(System.Reflection.Assembly.GetExecutingAssembly()).GetObject<String>("TinkerCOM.Scripts.CodeToInject.js");
            //injecto il codice ed aspetto che la funzione principale venga risolta
            JSResponse js = await browser.JS_Result(script);
            
        }


        /// <summary>
        /// Gestione dei messaggi in arrivo dal JS ( qualche arduino sta inviando via seriale un dato )
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Browser_JavascriptMessageReceived(object sender, object e)
        {
            /* TODO:
             * implementare una logica per messaggi "generici" ( modifica nome elemento / inizio simulazione ... ) 
             */
            List<Object> tmp = e._Cast<List<Object>>();
            OnSerialReceived?.Invoke((string)tmp[0], (byte)((int)tmp[1]));
        }


        /// <summary>
        /// Invia un messaggio all'arduino specificato
        /// </summary>
        /// <param name="Arduino">Nome dell'arduino</param>
        /// <param name="message">stringa da inviare</param>
        async public void SendMessage(String Arduino, String message)
        {
            await browser.JS_Result("(function(){return window.COM_API.SendString('"+ Arduino + "','"+ message.Escape() + "');})()");
        }
        /// <summary>
        /// Invia un messaggio all'arduino specificato
        /// </summary>
        /// <param name="Arduino">Nome dell'arduino</param>
        /// <param name="message">array di byte da inviare</param>
        async public void SendMessage(String Arduino, byte[] message)
        {    
            await browser.JS_Result("(function(){return window.COM_API.SendByteArray('" + Arduino + "'," + message.ToJsString() + ");})()");
        }
        /// <summary>
        /// Invia un messaggio all'arduino specificato
        /// </summary>
        /// <param name="Arduino">Nome dell'arduino</param>
        /// <param name="message">byte da inviare</param>
        async public void SendMessage(String Arduino, byte message)
        {
            await browser.JS_Result("(function(){return window.COM_API.SendByte('" + Arduino + "'," + message + ");})()");
        }


        /// <summary>
        /// Ritorna i nomi degli arduini presenti nello schema
        /// </summary>
        /// <returns></returns>
        async public Task<String[]> GetArduiniNames()
        {
            JSResponse r = await browser.JS_Result("(function(){return window.COM_API.GetArduiniNames();})()");
            return r.Result._Cast<List<object>>().Cast<String>().ToArray();
        }




    }
}
