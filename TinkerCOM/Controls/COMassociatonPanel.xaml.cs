﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExtendCSharp;
using TinkerCOM.Classes;

namespace TinkerCOM.Controls
{
    /// <summary>
    /// Logica di interazione per COMassociatonPanel.xaml
    /// </summary>
    public partial class COMassociatonPanel : UserControl
    {
        string NullCOM = "-";

        public COMassociatonPanel()
        {
            InitializeComponent();
            LoadCombo();
            COM_JS_Association.OnArduinoCOMchanged += COMassociatonPanel_OnArduinoCOMchanged;
        }



        public COMassociatonPanel(String ArduinoName)
        {
            InitializeComponent();
            this.ArduinoName = ArduinoName;
            LoadCombo();
            COM_JS_Association.OnArduinoCOMchanged += COMassociatonPanel_OnArduinoCOMchanged;
        }
        public COMassociatonPanel(String ArduinoName, String COM_port)
        {
            InitializeComponent();
            this.ArduinoName = ArduinoName;
            this.ArduinoCOM = COM_port;

            LoadCombo();
            COM_JS_Association.OnArduinoCOMchanged += COMassociatonPanel_OnArduinoCOMchanged;
        }
        private void COMassociatonPanel_OnArduinoCOMchanged(string ArduinoName, string ArduinoCOM)
        {
            if(ArduinoName!=_ArduinoName)
                LoadCombo();
        }

        private String _ArduinoName = "-";
        public String ArduinoName {
            get
            {

                return _ArduinoName;
            }
            set
            {
                _ArduinoName = value;
                label_arduinoName.SetContentInvoke(_ArduinoName);
            }
            
        }

        public String ArduinoCOM
        {
            get
            {
                return COM_JS_Association.GetCOM(_ArduinoName);
            }
            set
            {
                COM_JS_Association.SetCOM(_ArduinoName, value);
            }

        }



        private void LoadCombo()
        {
            SuspendSelectionChanged = true;

            combo_arduinoCOM.Items.Clear();
            String[] ps = COM_JS_Association.GetFreePorts();
           
            combo_arduinoCOM.Items.Add(new ComboBoxItem { Content = NullCOM });
            foreach (String s in ps)
            {
                combo_arduinoCOM.Items.Add(new ComboBoxItem { Content = s });
            }

            String c = COM_JS_Association.GetCOM(_ArduinoName);
            if(c!=null)
            {
                int index=combo_arduinoCOM.Items.Add(new ComboBoxItem { Content = c });
                combo_arduinoCOM.SelectedIndex = index;
            }
            else
            {
                combo_arduinoCOM.SelectedItem = ((ComboBoxItem)combo_arduinoCOM.Items[0]) ;
                combo_arduinoCOM.Text = ((ComboBoxItem)combo_arduinoCOM.Items[0]).Content.ToString();
            }

            SuspendSelectionChanged = false;

        }



        bool SuspendSelectionChanged = false;
        private void combo_arduinoCOM_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SuspendSelectionChanged)
                return;

            if ((ComboBoxItem)combo_arduinoCOM.SelectedItem == null)
                return;

            String content = (String)((ComboBoxItem)combo_arduinoCOM.SelectedItem).Content;
            if( content==NullCOM)
            {
                COM_JS_Association.SetCOM(_ArduinoName, null);
            }
            else if (!COM_JS_Association.SetCOM(_ArduinoName, content) )
            {
                combo_arduinoCOM.SelectedIndex = 0;
            }
        }
    }
}
