﻿
var COM_API = {
    ArduiniHashTable:{      //tabella utilizzata per velocizzare il recupero dell'arduino in base al nome
    },
    _init: function (win) {
        win.COM_API = this;
    },
    Loaded: function () {
        console.log("Caricato");

        //Bind lettura seriale
        var arduini = this.GetArduini();
        $.each(arduini, function (t, arduino) {
            if (arduino.breadboard().dynamicModel.stdout) {
                //"simulation_COM_API" è l'ID dell'hash map in cui viene inserita la funzione ( se metto la stessa che usa tinkercad vado a sostituire la sua funzione )
                arduino.breadboard().dynamicModel.stdout.onAdd("simulation_COM_API", function (t, i, carattere) {
                    COM_API.ReceiveChar(arduino, carattere);
                }, 10)
            }
        });
    },
    GetArduini: function () {
        return global.window.circuit_editor.circuitEditor.codePanel.programmableComponents;
    },
    GetArduiniNames: function () {

        //Crea l'hashtable da utilizzare per la comunicazione e ritorna la lista dei nomi degli arduini
        this.CreateArudiniHashTable();

        var arduini = this.GetArduini();
        var tmp = [];
        $.each(arduini, function (t, arduino) {
            tmp.push(arduino.name());
        });
        return tmp;

    },
    SendString: function (Arduino, Stringa) {
        Arduino = this.GetArduinoFromName(Arduino);
        var aaa = new Circuits.GuiEvent(Circuits.GuiEvents.POST_SERIAL_MESSAGE).element("element", Arduino.breadboard()).data("msg", Stringa);
        global.window.circuit_editor.stateMachine.handle(aaa);
    },
    SendStringHex: function (Arduino, StringaHex) {
        function hex2a(hexx) {
            var hex = hexx.toString();//force conversion
            var str = '';
            for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
                str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
            return str;
        }
        Arduino = this.GetArduinoFromName(Arduino);
        var data = hex2a(StringaHex);
        var aaa = new Circuits.GuiEvent(Circuits.GuiEvents.POST_SERIAL_MESSAGE).element("element", Arduino.breadboard()).data("msg", data);
        window.circuit_editor.stateMachine.handle(aaa);
    },
    SendByteArray: function (Arduino, byteArray) {
        Arduino = this.GetArduinoFromName(Arduino);
        var str = "";
        for (var i = 0; i < byteArray.length; i += 1)
            str += String.fromCharCode(byteArray[i]);
        var aaa = new Circuits.GuiEvent(Circuits.GuiEvents.POST_SERIAL_MESSAGE).element("element", Arduino.breadboard()).data("msg", str);
        window.circuit_editor.stateMachine.handle(aaa);
    },
    SendByte: function (Arduino, byte) {
        Arduino = this.GetArduinoFromName(Arduino);
        var str = String.fromCharCode(byte);
        var aaa = new Circuits.GuiEvent(Circuits.GuiEvents.POST_SERIAL_MESSAGE).element("element", Arduino.breadboard()).data("msg", str);
        window.circuit_editor.stateMachine.handle(aaa);
    },

    ReceiveChar: function (Arduino, Char) {
        CefSharp.PostMessage([Arduino.name(),Char.charCodeAt(0)]);
    },
    CreateArudiniHashTable: function () {
        this.ArduiniHashTable = {};
        tmpHash = this.ArduiniHashTable;
        var arduini = this.GetArduini();
        $.each(arduini, function (t, arduino) {
            tmpHash[arduino.name()] = arduino;
        });

    },
    GetArduinoFromName: function (name) {
        return this.ArduiniHashTable[name];
    },
};











(function () {

    'use strict';
    function docReady(fn) {
        if (document.readyState === "complete" || document.readyState === "interactive") {
            setTimeout(fn, 1);
        } else {
            document.addEventListener("DOMContentLoaded", fn);
        }
    }


    docReady(async function () {
        await WaitTinkecadLoading();
        COM_API._init(global.window);
        COM_API.Loaded();
    });

    //Attende in maniera asincrona il caricamento dei componenti di Tinkercad
    async function WaitTinkecadLoading() {
        return new Promise(resolve => {

            var intervall = setInterval(() => {
                //window.circuit_editor.circuitEditor.code_editor.refreshSerialMonitor
                if (typeof global.window.circuit_editor === 'undefined')
                    return;

                if (typeof global.window.circuit_editor.circuitEditor === 'undefined')
                    return;

                if (typeof global.window.circuit_editor.circuitEditor.codePanel === 'undefined')
                    return;

                if (typeof global.window.circuit_editor.circuitEditor.codePanel.programmableComponents === 'undefined')
                    return;

                if (typeof global.window.circuit_editor.circuitEditor.code_editor === 'undefined')
                    return;

                if (typeof global.window.circuit_editor.circuitEditor.code_editor.refreshSerialMonitor === 'undefined')
                    return;


                clearInterval(intervall);
                resolve('resolved');
            }, 500);

        });
    }
})();